package com.mlaguna.twitch;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "usuarios", schema = "twitch", catalog = "")
public class Usuario {
    private int id;
    private String nombre;
    private boolean prime;
    private List<Suscripcion> suscripciones;
    private Configuracion configuracion;
    private List<Streamer> streamers;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "prime")
    public boolean isPrime() {
        return prime;
    }

    public void setPrime(boolean prime) {
        this.prime = prime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return id == usuario.id &&
                prime == usuario.prime &&
                Objects.equals(nombre, usuario.nombre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, prime);
    }

    /**
     * Esta relacion es OneToMany ya que un usuario puede tener varias suscripciones activas
     * @return
     */
    @OneToMany(mappedBy = "usuario")
    public List<Suscripcion> getSuscripciones() {
        return suscripciones;
    }

    public void setSuscripciones(List<Suscripcion> suscripciones) {
        this.suscripciones = suscripciones;
    }

    /**
     * Esta relacion es OneToOne ya que un usuario solo puede tener una configuracion
     * @return
     */
    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id_usuario", nullable = false)
    public Configuracion getConfiguracion() {
        return configuracion;
    }

    public void setConfiguracion(Configuracion configuracion) {
        this.configuracion = configuracion;
    }

    /**
     * Esta relacion es ManyToMany ya que varios usuarios pueden estar suscritos a varios streamers
     * @return
     */
    @ManyToMany
    @JoinTable(name = "usuarios_streamers", catalog = "", schema = "twitch", joinColumns = @JoinColumn(name = "id_streamer", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false))
    public List<Streamer> getStreamers() {
        return streamers;
    }

    public void setStreamers(List<Streamer> streamers) {
        this.streamers = streamers;
    }
}
