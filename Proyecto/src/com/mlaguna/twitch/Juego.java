package com.mlaguna.twitch;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "juegos", schema = "twitch", catalog = "")
public class Juego {
    private int id;
    private String titulo;
    private String categoria;
    private List<Streamer> streamers;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titulo")
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Basic
    @Column(name = "categoria")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Juego juego = (Juego) o;
        return id == juego.id &&
                Objects.equals(titulo, juego.titulo) &&
                Objects.equals(categoria, juego.categoria);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titulo, categoria);
    }

    /**
     * Esta relacion es ManyToMany porque varios juegos pueden ser retransmitidos por varios streamers.
     * @return
     */
    @ManyToMany(mappedBy = "juegos")
    public List<Streamer> getStreamers() {
        return streamers;
    }

    public void setStreamers(List<Streamer> streamers) {
        this.streamers = streamers;
    }
}
