package com.mlaguna.twitch;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "streamers", schema = "twitch", catalog = "")
public class Streamer {
    private int id;
    private String nombre;
    private int seguidores;
    private List<Usuario> usuarios;
    private List<Juego> juegos;
    private List<Clip> clips;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "seguidores")
    public int getSeguidores() {
        return seguidores;
    }

    public void setSeguidores(int seguidores) {
        this.seguidores = seguidores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Streamer streamer = (Streamer) o;
        return id == streamer.id &&
                seguidores == streamer.seguidores &&
                Objects.equals(nombre, streamer.nombre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, seguidores);
    }

    /**
     * Esta relacion es ManyToMany porque varios streamers pueden tener varios suscriptores (usuarios)
     * @return
     */
    @ManyToMany(mappedBy = "streamers")
    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * Esta relacion es ManyToMany porque varios streamers pueden retransmitir varios juegos
     * @return
     */
    @ManyToMany
    @JoinTable(name = "juegos_streamers", catalog = "", schema = "twitch", joinColumns = @JoinColumn(name = "id_juego", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_streamer", referencedColumnName = "id", nullable = false))
    public List<Juego> getJuegos() {
        return juegos;
    }

    public void setJuegos(List<Juego> juegos) {
        this.juegos = juegos;
    }

    /**
     * Esta relacion es OneToMany porque un streamer puede tener varios clips
     * @return
     */
    @OneToMany(mappedBy = "streamer")
    public List<Clip> getClips() {
        return clips;
    }

    public void setClips(List<Clip> clips) {
        this.clips = clips;
    }
}
