package com.mlaguna.twitch;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Suscripcion {
    private int id;
    private int cantidad;
    private Date fechaLimite;
    private Usuario usuario;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "fecha_limite")
    public Date getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(Date fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Suscripcion that = (Suscripcion) o;
        return id == that.id &&
                cantidad == that.cantidad &&
                Objects.equals(fechaLimite, that.fechaLimite);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad, fechaLimite);
    }

    /**
     * Esta relacion es ManyToOne ya que una suscripcion es de un usuario
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
