package com.mlaguna.twitch;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Configuracion {
    private int id;
    private boolean tema;
    private String foto;
    private boolean oculto;
    private Usuario usuario;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "tema")
    public boolean isTema() {
        return tema;
    }

    public void setTema(boolean tema) {
        this.tema = tema;
    }

    @Basic
    @Column(name = "foto")
    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Basic
    @Column(name = "oculto")
    public boolean isOculto() {
        return oculto;
    }

    public void setOculto(boolean oculto) {
        this.oculto = oculto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Configuracion that = (Configuracion) o;
        return id == that.id &&
                tema == that.tema &&
                oculto == that.oculto &&
                Objects.equals(foto, that.foto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tema, foto, oculto);
    }

    /**
     * Esta relacion es OneToOne porque una configuracion solo la puede tener un usuario
     * @return
     */
    @OneToOne(mappedBy = "configuracion")
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
