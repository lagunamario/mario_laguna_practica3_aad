package com.mlaguna.twitch;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Clip {
    private int id;
    private String titulo;
    private int duracion;
    private boolean privado;
    private Streamer streamer;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titulo")
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Basic
    @Column(name = "duracion")
    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    @Basic
    @Column(name = "privado")
    public boolean isPrivado() {
        return privado;
    }

    public void setPrivado(boolean privado) {
        this.privado = privado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clip clip = (Clip) o;
        return id == clip.id &&
                duracion == clip.duracion &&
                privado == clip.privado &&
                Objects.equals(titulo, clip.titulo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titulo, duracion, privado);
    }

    /**
     * Esta relacion es ManyToOne ya que un clip tiene un streamer
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "id_streamer", referencedColumnName = "id")
    public Streamer getStreamer() {
        return streamer;
    }

    public void setStreamer(Streamer streamer) {
        this.streamer = streamer;
    }
}
