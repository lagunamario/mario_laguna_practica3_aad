create database twitch;
use twitch;

create table usuarios(
id int primary key auto_increment not null,
nombre varchar(15),
prime boolean
);

create table streamers(
id int primary key not null auto_increment,
nombre varchar(15),
seguidores int
);

create table juegos(
id int primary key not null auto_increment,
titulo varchar(45),
categoria varchar(30)
);

create table suscripcion(
id int primary key not null auto_increment,
cantidad int,
fecha_limite date,
id_usuario int references usuarios(id)
);

create table clip(
id int primary key not null auto_increment,
titulo varchar(25),
duracion int,
privado boolean,
id_streamer int references streamers(id)
);

create table configuracion(
id int primary key not null auto_increment,
tema boolean,
foto varchar(100),
oculto boolean,
id_usuario int references usuarios(id)
);

create table juegos_streamers(
id_juego int,
id_streamer int,
primary key(id_juego, id_streamer)
);

create table usuarios_streamers(
id_usuario int,
id_streamer int,
primary key(id_usuario, id_streamer)
);
